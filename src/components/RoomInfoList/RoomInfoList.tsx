import { FC } from 'react';
import { useTranslation } from 'next-i18next';

import styles from './RoomInfoList.module.scss';
import { RoomInfo, Props as RoomInfoProps } from './RoomInfo/RoomInfo';
import { defaultRoomInfoProp } from './constants';

type Props = {
  data?: RoomInfoProps[];
};

const RoomInfoList: FC<Props> = ({ data = defaultRoomInfoProp }) => {
  const { t } = useTranslation('room-details');
  return (
    <div className={styles.RoomInfoList}>
      <h2 className={styles.RoomInfoList__title}>{t('roomInfo')}</h2>
      {data.map(({ title, text, img }, key) => {
        return (
          <RoomInfo title={title} text={text} key={String(key)} img={img} />
        );
      })}
    </div>
  );
};

export { RoomInfoList };
