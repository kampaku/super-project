import { getStorage, ref, getDownloadURL } from 'firebase/storage';

class ApiImage {
  static storage = getStorage();

  static getLink = async (id: number) => {
    const url = await getDownloadURL(ref(this.storage, `rooms/room-${id}.jpg`));
    return url;
  };
}

export default ApiImage;
